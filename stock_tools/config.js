/**
 * 股票小助手配置文件
 */

// 此处主机域名修改成腾讯云解决方案分配的域名
//var host = 'https://www.ikos.cn/index.php?s=/api';
var host = 'https://www.ikos.cn/wechat';
var config = {

    // API请求地址
    service: {
        host:host,
        // 登录地址，用于建立会话
        loginUrl: `${host}/user/login/`,
        // 测试的请求地址，用于测试会话
        requestUrl: `${host}/`
    },
    name: "股民小助手©爱客开源",
    uniacid: "10001", // 默认即可，勿填
};

module.exports = config;
//index.js
var qcloud = require('../../vendor/wafer2-client-sdk/index')
var config = require('../../config')
var util = require('../../utils/util.js')

let App = getApp();

Page({
    /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
  },
  onLoad:function(opts){
    
  },
  onShow:function(){
      // 获取当前用户信息
      this.getUserDetail();
  },

  /**
* 获取当前用户信息
*/
  getUserDetail: function () {
        let _this = this;
        App._get('user/info', {}, function (result) {
          if (result.code === 1) {
            _this.setData(result.data);
          } else {
            App.showError(result.msg);
          }
        });
  },


})

//app.js
var qcloud = require('./vendor/wafer2-client-sdk/index')
var config = require('./config')

App({
  apiUrl: null, // api地址
  siteConfig: config,
  onLaunch: function () {
        //设置请求地址
        qcloud.setLoginUrl(config.service.loginUrl);
        this.apiUrl = config.service.requestUrl;
  },
 
  /**
* 当小程序启动，或从后台进入前台显示，会触发 onShow
*/
  onShow: function (options) {
    // 获取小程序基础信息
    this.getWxappBase(function (wxapp) {
      console.log('token:', wx.getStorageSync('token'))
      // 设置navbar标题、颜色
      //wx.setNavigationBarColor({
        //frontColor: wxapp.navbar.top_text_color,
        //backgroundColor: wxapp.navbar.top_background_color
     // })
    });
  },
  /**
    * 获取小程序基础信息
    */
  getWxappBase: function (callback) {
    let App = this;
    App._get('wxapp/base', {}, function (result) {
      console.log('getWxappBase:', result);
      if (result.code === 1) {
        // 记录小程序基础信息
        wx.setStorageSync('wxapp', result.data.wxapp);
        callback && callback(result.data.wxapp);
      } else {
        App.showError(result.msg);
      }
    }, false, false);
  },
  /**
    * 显示成功提示框
    */
  showSuccess: function (msg, callback) {
    wx.showToast({
      title: msg,
      icon: 'success',
      success: function () {
        callback && (setTimeout(function () {
          callback();
        }, 1500));
      }
    });
  },

  /**
   * 显示失败提示框
   */
  showError: function (msg, callback) {
    wx.showModal({
      title: '友情提示',
      content: msg,
      showCancel: false,
      success: function (res) {
        // callback && (setTimeout(function() {
        //   callback();
        // }, 1500));
        callback && callback();
      }
    });
  },
  /**
  * get请求
  */
  _get: function (url, data, success, fail, complete) {
    wx.showNavigationBarLoading();
    let App = this;
    // 构造请求参数
    data = data || {};
    data.wxapp_id = App.siteConfig.uniacid;
    // 构造get请求
    let request = function () {
      data.token = wx.getStorageSync('token');
      wx.request({
        url: App.apiUrl + url,
        header: {
          'content-type': 'application/json'
        },
        data: data,
        success: function (res) {
          if (res.statusCode !== 200 || typeof res.data !== 'object') {
              console.log(res);
              App.showError('网络请求出错');
              return false;
          }
          if (res.data.code === -1) {
              // 登录态失效, 重新登录
              wx.hideNavigationBarLoading();
              App.doLogin();
          } else if (res.data.code === 0) {
              App.showError(res.data.msg);
              return false;
          } else {
              success && success(res.data);
          }
        },
        fail: function (res) {
            App.showError(res.errMsg, function () {
              fail && fail(res);
            });
        },
        complete: function (res) {
          wx.hideNavigationBarLoading();
          complete && complete(res);
        }
      });
    }
    // 判断是否需要验证登录
    request();
  },
  /**
 * 执行用户登录
 */
  doLogin: function (request) {
    // 跳转授权页面
    wx.navigateTo({
      url: "/pages/login/login"
    });
  },    
  /**
   * 设置当前页面标题
   */
  setTitle: function () {
    let App = this,wxapp;
    wxapp = wx.getStorageSync('wxapp') 
    if (wxapp && wxapp.navbar.wxapp_title!=undefined) {
      wx.setNavigationBarTitle({
        title: wxapp.navbar.wxapp_title
      });
    }
  },
  /**
    * post提交
    */
  _post_form: function (url, data, success, fail, complete) {
    wx.showNavigationBarLoading();
    let App = this;
    data.wxapp_id = App.siteConfig.uniacid;
    data.token = wx.getStorageSync('token');
    wx.request({
      url: App.apiUrl + url,
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      data: data,
      success: function (res) {
        if (res.statusCode !== 200 || typeof res.data !== 'object') {
            App.showError('网络请求出错');
            return false;
        }
        if (res.data.code === -1) {
            // 登录态失效, 重新登录
            App.doLogin(function () {
              App._post_form(url, data, success, fail);
            });
          return false;
        } else if (res.data.code === 0) {
            App.showError(res.data.msg, function () {
              fail && fail(res);
            });
            return false;
        }
        success && success(res.data);
      },
      fail: function (res) {
          // console.log(res);
          App.showError(res.errMsg, function () {
            fail && fail(res);
          });
      },
      complete: function (res) {
          wx.hideLoading();
          wx.hideNavigationBarLoading();
          complete && complete(res);
      }
    });
  },



})